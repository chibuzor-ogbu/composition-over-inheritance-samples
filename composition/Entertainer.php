<?php
/**
 * @author Chibuzor Ogbu <chibuzorogbu@gmail.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */

namespace Composition;

class Entertainer
{

    private InstrumentInterface $instrument;


    /**
     * The type of entertainer is dependant on the instrument he plays and not the hierarchy it belongs to. A drummer will perform with drums.
     * Entertainer constructor.
     * @param  InstrumentInterface  $instrument
     */
    public function __construct(InstrumentInterface $instrument)
    {
        $this->instrument = $instrument;
    }


    public function perform(?InstrumentInterface $instrument = null)
    {
        return $instrument ? $this->instrument->sound().' '.$instrument->sound() : $this->instrument->sound();
    }
}

