<?php
/**
 * @author Chibuzor Ogbu <chibuzorogbu@gmail.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */
namespace Composition;

class Guitar implements InstrumentInterface
{

    public function sound()
    {
        return 'Gin! Gin! Gin!!!';
    }
}
