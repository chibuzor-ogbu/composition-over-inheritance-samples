<?php
/**
 * @author Chibuzor Ogbu <chibuzorogbu@gmail.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */
namespace Composition;

interface InstrumentInterface
{
    // a clean interface defines what any instrument object can do

    public function sound();

}
