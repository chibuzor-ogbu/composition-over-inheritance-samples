<?php
/**
 * @author Chibuzor Ogbu <chibuzorogbu@gmail.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */
namespace Composition;

class Drum implements InstrumentInterface
{

    // a drum can only make a sound when beat.
    // adding effects to the drum sound isn't something the drum should worry about.
    // Hint: you only need to pass the drum sound to an effects interface
    public function sound()
    {
        return "Dum! Dum! Dum! Dum!";
    }
}
