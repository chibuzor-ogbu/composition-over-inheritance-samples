<?php
/**.
 * @author Chibuzor Ogbu <chibuzor.ogbu@createmusicgroup.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */


namespace Inheritance;


class SingingDrummer extends Singer
{
    // overriding the base method could lead to issues
    // when the base class decides to change its declaration
    // Entertainer::perform($dance)
    public function perform()
    {
        return $this->drums() . ' ' . $this->sings();
    }


    // duplication of code
    public function drums(){
        return 'Dum! Ke!!... Dum! Ke!!';
    }
}

/**
 * Another example demonstrating how singing drummer Could also be made to extend the base class directly
 * Class SingingDrummer
 * @package Inheritance
 */
class SingingDrummer extends Entertainer
{
    // overriding the base method could lead to issues
    // when the base class decides to change its declaration
    // e.g Entertainer::perform($withDance)
    public function perform()
    {
        return $this->drums() . ' ' . $this->sings();
    }


    // duplication of code
    public function drums(){
        return 'Dum! Ke!!... Dum! Ke!!';
    }

    // duplication of code
    public function sings()
    {
        return 'La La La La';
    }
}

class SingingDrummer extends Entertainer
{
    public function __construct()
    {
        $this->instrument = 'La La La La Dum! Ke!!... Dum! Ke!!';
    }

    // what if i only wanted the singing drummer to sing without drumming on the hook of the song?
    // obviously not flexible
    public function perform()
    {
        return $this->singsAndDrum();
    }


    // ah yet another method
    public function singsAndDrum()
    {
        return $this->instrument;
    }
}
