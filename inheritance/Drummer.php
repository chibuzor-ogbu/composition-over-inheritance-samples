<?php
/**
 * @author Chibuzor Ogbu <chibuzorogbu@gmail.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */


namespace Inheritance;


class Drummer extends Entertainer
{
    public function __construct()
    {
        $this->instrument = 'Dum! Ke!!... Dum! Ke!!';
    }

    public function drums(){
        return  $this->instrument;
    }

    public function perform()
    {
        return $this->drums();
    }
}
