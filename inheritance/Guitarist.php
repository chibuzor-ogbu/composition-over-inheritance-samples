<?php
/**
 * @author Chibuzor Ogbu <chibuzor.ogbu@createmusicgroup.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */


namespace Inheritance;


class Guitarist extends Entertainer
{

    public function __construct()
    {
        $this->instrument = 'Gin Gin Gweeen!!!';
    }


    /**
     * Error: Declaration of must be compatible with base class thus adding a new effects to the guitar sound isn't trivial
     * when performing
     * @param  \Closure  $someSpecialEffect
     * @return mixed
     */
    public function perform(\Closure $someSpecialEffect)
    {
       return $someSpecialEffect($this->instrument);
    }
}
