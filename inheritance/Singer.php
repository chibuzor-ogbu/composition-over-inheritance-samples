<?php
/**
 * @author Chibuzor Ogbu <chibuzor.ogbu@createmusicgroup.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */


namespace Inheritance;


class Singer extends Entertainer
{
    public function __construct()
    {
        $this->instrument = 'La La La La';
    }


    public function sings()
    {
        return $this->instrument;
    }

    public function perform()
    {
        return $this->sings();
    }
}
