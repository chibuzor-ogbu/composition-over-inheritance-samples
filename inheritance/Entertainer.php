<?php
/**
 * @author Chibuzor Ogbu <chibuzor.ogbu@createmusicgroup.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */
namespace Inheritance;

abstract class Entertainer
{
    protected $instrument;

    abstract public function perform();

}
