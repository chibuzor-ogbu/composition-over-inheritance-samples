<?php
/**
 * @author Chibuzor Ogbu <chibuzor.ogbu@createmusicgroup.com>
 * @created 2020-11-12
 * @copyright ©2020. All rights reserved.
 */

use Inheritance\Drummer;
use Inheritance\Guitarist;
use Inheritance\Singer;
use Inheritance\SingingDrummer;

use Composition\Entertainer;
use Composition\Voice;
use Composition\Drum;
use Composition\Guitar;

require "vendor/autoload.php";

// Using Inheritance as example

$singer = new Singer();
$drummer = new Drummer();
$guitarist = new Guitarist();
$singingDrummer = new SingingDrummer();
$effect = static function ($sound){
    return str_shuffle($sound);
};

// execute inheritance
echo $singer->perform()  .PHP_EOL;
echo $singingDrummer->perform()  .PHP_EOL;
echo $drummer->perform()    .PHP_EOL;
echo $guitarist->perform($effect)  .PHP_EOL; // oops the guitarist performance can't be extended with an effect



// Using Composition as example

$singer = new Entertainer(new Voice());
$drummer = new Entertainer(new Drum());
$guitarist = new Entertainer(new Guitar());

// execute composition
echo $singer->perform()  .PHP_EOL;
echo $singer->perform(new Drum())  .PHP_EOL;  // a singing drummer easily doable with composition, this could also be just passing in an effect composition
echo $drummer->perform()    .PHP_EOL;
echo $guitarist->perform()  .PHP_EOL;


